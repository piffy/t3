# TEAMWORK

## Impegno
Ruoli ufficiali (ore di lavoro non dichiarate): 
Schiavone Antonio (Programmatore JAVA)  
Macchi Alberto    (Programmatore JAVA/JS)  
Silvestre Samuele (Addetto all'HTML)  
Mezzetti Mattia   (Programmatore/Tester)  
Lamorgese Angelo  (Public relations e Controllore) 



## Git 
Macchi 37 (Parser.java,Requester.java, Borsa.java, Utils.java, Retrospective.txt)
Silvestre 49
Mattia Mezzetti 19
schiavoanto 34
Angelo Lamorgese 3 (Utils.java)

Web upload: 0/144

## Retrospective
Entrambe eseguite in modo superficiale e di scarsa efficacia

## Sintesi

L'immagine dei gruppo è quella di un aspetto esteriore piuttosto compatto, ma per del quale ogni componente ha una percezione diversa e personale su tutto o quasi. Taluni pensano che il gruppo si sia ben organizzato, altri meno; alcuni pensano sia stata un'esperienza utile, altri meno; stesse opinioni discordanti sulla progettazione. Perfino, sul fatto che il progetto sia riuscito o meno. Si concorda solo sull'irritazione generale dovuta alla "insistenza" degli aggiornamenti e sulla incomunicabilità con cliente e docenti - su questo fatto credo dobbiate farvene una ragione ed adattarvi, il mondo reale sarà generalmente peggio.

