User stories totali (fatte) 8/8 (trello)
Commento US: Generalmente accettabili. Manca qualsiasi riferimento al fatto che l'input derivi dalla chiamate rest al server; le storie sono state aggiornate per eliminare quelle errate, ma non ne sono state aggiunte o chiarite - per quanto si può dedurre dal log di trello.Stimate: no, Prioritizzate: probabilmente si.
Organizzazione in sprint (velocity): no -   media 2.6 US al giorno, ma in realtà fatte quasi tutte il primo giorno-

CRITICA
As a user, I want the program to save all info on an HTML file
	OK
As a user, I want to be able to open the HTML on my browser
	DOH... A DUMB USER? NOT REALLY YOUR PROBLEM
As a user, I would like to have a chart with the performance of the stock
	OK
As a user, I want to be able to know the maximum I can get selling stocks
	BADLY WORDED. WHICH STOCKS?
As a user, I want to be able to know how much the stock has increased/decreased in a certain period.
	OK
As a user, I want to be able to know how much the stock has increased in a certain period, in percentage.
	OK
As a user, I want to be able to know the most frequent stock values.
	NOT CLEAR
As a user, I want the page to auto-update itself frequently
	"FREQUENTLY IS TOO VAGUE"

