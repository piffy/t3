Valutazione Torricelli: 4.36 (il migliore)
Evoluzione progettazione:  Lo schema si è evoluto nel corso del progetto, seguendo la struttura del codice. 
Documentazione JavaDoc non è male.

Critica UML
La progettazione prevede 4 oggetti, e lo schema è formalmente corretto.
Il codice finale rispecchia l'UML, tranne differenze (minori) nella classe Borsa. 


Critica Codice
Static code Analysis: B
Utils.java: classe "leggera" di utilità

Requester.java: richiede dati via internet. Viste le evidenti ripetizioni di codice, si poteva organizzare meglio, ma l'idea è razionale. 

Parser.java: separatore di dati. Ben organizzata.

Borsa.java: di fatto il main(). Probabilmente si poteva spartire in 2-3 funzioni, cosa che forse avrebbe permesso di identificare prima il problema del refresh. L'apertura automatica del browser è interessante, ancorché non richiesta.








 




	
	
