# DW B T3 BORSA

Thanks to a finance API and to Javascript, HTML and Java, our goal is to have an HTML page which will show a determined company's stock status, with the relative graphs.
On the code part, we're using Java for making API calls and Javascript to power the HTML page and to create the requested graphs. The HTML page will update itself.
All of the code is documented.

# COMPONENTI
Schiavone Antonio (Programmatore JAVA)  
Macchi Alberto    (Programmatore JAVA/JS)  
Silvestre Samuele (Addetto all'HTML)  
Mezzetti Mattia   (Programmatore/Tester)  
Lamorgese Angelo  (Public relations e Controllore)  

# LINK UTILI
Progetto: https://docs.google.com/document/d/1n9ppuHmVRj0__-WUvkqyaIKqdXXzqvrCABoGmOXEbH4/edit  
Trello Invitation: https://trello.com/invite/b/oQZaqxGK/9629f3e1b5bcab7e7ffde7e2d1b4f3b0/dwbt3  
Trello: https://trello.com/b/oQZaqxGK/dwbt3  
Diario giornaliero: https://docs.google.com/document/d/1SsrjFEJCyeOkor8G-fXW_YpeWAkqb1a_vzsCBOqyRls/edit?usp=sharing  
Sprint Backlog: https://docs.google.com/document/d/1qor9DYcp82FQprzCT_-CI1L8WnHNcVqXz6f2VHBsWxE/edit?usp=sharing  
Slack Channel: https://app.slack.com/client/T01AZPNGA4C/C02KT4R8544
